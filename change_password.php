   <?php
require_once 'session.php';
if($_SESSION['role']!='s')
header('Location:index.php?id=Login With Proper Credentials to Continue');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/footer1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <?php require_once 'top_panel_student.php';?>

    <!-- Begin page content -->
    <div class="container">
            

       <div class="row-fluid">
     <h3> Change Password</h3>
<form class="form-horizontal" method="post"  action="change_password1.php">
<table class="table table-bordered" >


<tr><td>Current Password</td><td><input type="password" name="curr_password" title="Password is case sensitive"></input></td></tr>

<tr><td>New Password</td><td><input type="password" name="password" title="Password is case sensitive"></input></td></tr>
<tr><td>Re-enter New Password</td><td><input type="password" name="re-password" title="Password is case-sensitive"></input></td></tr>
<tr><td colspan="4"><center><input type="submit" class="btn btn-primary" value="submit" name="submit" ></tr>
</table>

</form>

</div>
  








    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">created with all love in the world by @Ankit and @Osho.</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>



