<?php
require_once 'session.php';
if($_SESSION['role']!='t')
header('Location:index.php?id=Login With Proper Credentials to Continue');
?>
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    
    <!-- Custom styles for this template -->
    <link href="css/footer1.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
   <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">Code-Y-Gen</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="admin_cyg.php">Home</a></li>
              <li><a href="admin_see_all.php">Members</a></li>
              <li><a href="admin_see_all.php">Members</a></li>
              <li><a href="change_password_admin.php">Change Password</a></li>
              <li><a href="logout.php"><?php echo $_SESSION['name'];?>(Logout)</a></li>
              </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>


    <!-- Begin page content -->
    <div class="container">

<?php

 require_once ('include/database.php');
 $q = "select * from members where id=:id";
$s = $dbh-> prepare ($q);
$s-> bindParam(':id', $_GET['id']);


$s-> execute();


  $r = $s-> fetch(PDO::FETCH_ASSOC);

if($r['status']=="")
{

?>


<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Warning!</strong> Your submission's Evaluation is under progress. It will soon be Verfied and you'll be awarded grades/marks
</div>

<?php
}
$tot=0;


if($r['message']!="")
{

?>

<div class="alert alert-success">
 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Message(from Admin)!</strong> <?php echo $r['message'] ?>

</div>

<?php
}
?>

<table class="table table-striped ">
  <tr><th>Activity Name</th> <th>Hours(hr) </th><th> Details you submitted</th>
  <tr><td >Ignitron `13<td>15<td><?php echo $r['1']; if($r['1']!="") $tot=$tot+15;  ?>
  <tr><td>Hosting and runnig a website<td>30<td><?php echo $r['2']; if($r['2']!="") $tot=$tot+30;  ?>
  <tr><td>Developing any mobile app<td>30<td><?php echo $r['3']; if($r['3']!="") $tot=$tot+30;  ?>
  <tr><td>PHP Fest<td>20<td><?php echo $r['4']; if($r['4']!="") $tot=$tot+20;  ?>
  <tr><td>Organizing Idea Fest<td>15<td><?php echo $r['5']; if($r['5']!="") $tot=$tot+15;  ?>
  <tr><td>Software Expo<td>10<td><?php echo $r['6'];  if($r['6']!="") $tot=$tot+10; ?>
  <tr><td>Organizing Technical Training<td>10<td><?php echo $r['7']; if($r['7']!="") $tot=$tot+10;  ?>
  <tr><td>Organizing Mock Interview<td>10<td><?php echo $r['8']; if($r['8']!="") $tot=$tot+10;  ?>
  <tr><td>Organizing Online Test<td>10<td><?php echo $r['9']; if($r['9']!="") $tot=$tot+10;  ?>
  <tr><td>Tornado `14<td>Not Available<td><?php echo $r['10']; if($r['10']!="") $tot=$tot+15;   ?>
</table>

<form role="form" method="POST" class="form-horizontal" action="give_credits.php?id=<?php echo $_GET['id']; ?>">
  <div class="form-group">
    <label for="exampleInputEmail1">Hours</label>
    <input type="text" name="hrs"class="form-control" id="exampleInputEmail1" placeholder="Enter Hours awarded" value="<?php echo $tot; ?>" >
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Message (if any)</label>
    <textarea  class="form-control" name="message" placeholder="Enter message if you have any"></textarea>
  </div>
  <div class="radio">
  <label>
    <input type="radio" name="optionsRadios" id="optionsRadios1" value="approved">
    Approve
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="optionsRadios" id="optionsRadios2" value="reject">
    Reject</label>
</div>
 
  <button type="submit" class="btn btn-default">Submit</button>
</form>

</div>



    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">created with all love in the world by @Ankit and @Osho.</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
