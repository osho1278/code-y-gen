   <?php
require_once 'session.php';
if($_SESSION['role']!='t')
header('Location:index.php?id=Login With Proper Credentials to Continue');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

    <!-- Custom styles for this template -->
    <link href="css/footer1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">Code-Y-Gen</a>
          <div class="nav-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="admin_cyg.php">Home</a></li>
              <li><a href="admin_see_all.php">Members</a></li>
              <li><a href="admin_see_all.php">Members</a></li>
              <li><a href="change_password_admin.php">Change Password</a></li>
              <li><a href="logout.php"><?php echo $_SESSION['name'];?>(Logout)</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>


    <!-- Begin page content -->
    <div class="container">
            

       <div class="row-fluid">
     <h3> Change Password</h3>
<form class="form-horizontal" method="post"  action="change_password1.php">
<table class="table table-bordered" >


<tr><td>Current Password</td><td><input type="password" name="curr_password" title="Password is case sensitive"></input></td></tr>

<tr><td>New Password</td><td><input type="password" name="password" title="Password is case sensitive"></input></td></tr>
<tr><td>Re-enter New Password</td><td><input type="password" name="re-password" title="Password is case-sensitive"></input></td></tr>
<tr><td colspan="4"><center><input type="submit" class="btn btn-primary" value="submit" name="submit" ></tr>
</table>

</form>

</div>
  








    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">created with all love in the world by @Ankit and @Osho.</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>



