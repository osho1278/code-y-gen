<?php
require_once 'session.php';
if($_SESSION['role']!='s')
header('Location:index.php?id=Login With Proper Credentials to Continue');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/jquery.toastmessage.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/footer1.css" rel="stylesheet">

    <script src="resources/jquery.toastmessage.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <?php require_once 'top_panel_student.php';?>
    <!-- Begin page content -->
    <div class="container">

<?php

 require_once ('include/database.php');
 $id=$_SESSION['id'];
 $q = "select * from members where id='$id'";
$s = $dbh-> prepare ($q);
// $s-> bindParam(':j', $j);


$s-> execute();


  $r = $s-> fetch(PDO::FETCH_ASSOC);
  if(isset($_GET['id']))
{

?>



<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Gracias!</strong> Your submission has been recoded !!!
</div>
<?php
}

if($r['status']=="pending")
{

?>



<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Heads Up!</strong> Your submission's Evaluation is under progress. It will soon be Verfied and you'll be awarded grades/marks
</div>

<?php
}
if($r['message']!="")
{

?>

<div class="alert alert-info">
 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Message(from Admin)!</strong> <?php echo $r['message']; ?>

</div>

<?php
}
?>


<?php

if($r['status']=="approved")
{


 echo "<script>var a=1;</script>";


}
if($r['status']=="reject")
{


 echo "<script>var a=2;</script>";


}
?>


<table class="table table-striped ">
  <tr><th>Activity Name</th> <th>Hours(hr) </th><th> Details you submitted</th>
  <tr><td >Ignitron `13<td>15<td><?php echo $r['1'];  ?>
  <tr><td>Hosting and runnig a website<td>30<td><?php echo $r['2'];  ?>
  <tr><td>Developing any mobile app<td>30<td><?php echo $r['3'];  ?>
  <tr><td>PHP Fest<td>20<td><?php echo $r['4'];  ?>
  <tr><td>Organizing Idea Fest<td>15<td><?php echo $r['5'];  ?>
  <tr><td>Software Expo<td>10<td><?php echo $r['6'];  ?>
  <tr><td>Organizing Technical Training<td>10<td><?php echo $r['7'];  ?>
  <tr><td>Organizing Mock Interview<td>10<td><?php echo $r['8'];  ?>
  <tr><td>Organizing Online Test<td>10<td><?php echo $r['9'];  ?>
  <tr><td>Tornado `14<td>Not Available<td><?php echo $r['10'];  ?>
</table>

</div>





    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">created with all love in the world by @Ankit and @Osho.</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="js/jquery.js"></script>

    <script src="resources/jquery.toastmessage.js"></script>
     
<script>
if(a==1)
{
	//var a=100;
	//alert(a);
	$().toastmessage('showToast', {
	text     : 'Your submissions have been approved by our Admin. <br> You have got <?php echo "".$r['points']."";  ?> hrs  as a credit.<br>Corresponding grades will be displayed later.',
    sticky   : true,
    type     : 'success'
});
//$().toastmessage('showNoticeToast', 'some message here');

 }
 if(a==2)
{
	$().toastmessage('showToast', {
	text     : 'Your submissions have been rejected by our Admin. Please refill your correct details and submit for evaluation again.',
    sticky   : true,
    type     : 'error'
});
//$().toastmessage('showNoticeToast', 'some message here');
 }
</script>

    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
