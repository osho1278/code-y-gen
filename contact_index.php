<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Code-Y-Gen <span style="color:yellow">Members</span> Arena</h3>
              <ul class="nav masthead-nav">
                <li ><a href="index.php">Home</a></li>
                <li class="active"><a href="contact_index.php">Contact</a></li>
              </ul>
            </div>
          </div>

  <div class="inner cover">
              <h1>For Any Information</h1>
      
      <p class="lead">Please contact any of club co-ordinators <br>or<br> Prof Sathis Kumar (MBA BLOCK 4th floor).</p>
      <p><h2><code>Email : sathiskumar.b@vit.ac.in
      
      </code></h2></p>
  
</div>





<div class="mastfoot">
            <div class="inner">
              <p>Created with all love in the world by @Ankit and @Osho.
              </p>
            </div>
          </div>

    </div>

          </div>



</div>



    </div>




          </div>
          </div>
          </div>


          
        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/doc.js"></script>
  </body>
</html>
