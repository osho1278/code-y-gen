<!DOCTYPE html>
<?php
require_once 'session.php';
if($_SESSION['role']!='t')
header('Location:index.php?id=Login With Proper Credentials to Continue');
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
<!--      <link href="css/bootstrap.min.css" rel="stylesheet"> -->
<style>
#color:hover
{
  color : Purple;
  font-size:18px;
}

</style>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    
    <!-- Custom styles for this template -->
    <link href="css/footer1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
   <?php
                                             require_once ('include/database.php');

                                            $odd=1;
                                            $user_id=$_SESSION['id'];
                                            $stmt=$dbh->prepare("select * from members");
                                            $stmt->execute();
                                            ?>

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">Code-Y-Gen</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li ><a href="admin_cyg.php">Home</a></li>
              <li class="active"><a href="admin_see_all.php">Members</a></li>
               <li><a href="admin_see_all.php">Members</a></li>
              <li><a href="change_password_admin.php">Change Password</a></li>
              <li><a href="logout.php"><?php echo $_SESSION['name'];?>(Logout)</a></li>
              </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <!-- Begin page content -->
    
    <div class="span2">
</div>

    <div class="span8">
    <?php
if(isset($_GET['num']))
{
$r=$_GET['num'];

$stmt1=$dbh->prepare("select * from members where id=:a");
$stmt1-> bindParam(':a', $r);
  $stmt1->execute();
$r = $stmt1-> fetch(PDO::FETCH_ASSOC);
?>
<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Success!</strong> details updated for User <?php echo $r['name'];?>
</div>

<?php
}
?>

    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Club Members List</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                
            
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Member Name</th>
                                                
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            
                                            while($r = $stmt->fetch())
                                            {
                                            ?>
                                               
                                          <tr class="odd gradeX" id="color" onclick="window.location='view_submission.php?id=<?php echo $r['id'];?>'">
                                                    <td><?php echo $r['id'];?></td>
                                                    <td><?php echo $r['name'];?></td>
                                                    <!-- <td><div class="progress progress-striped active">
  <div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $r['percentage']."%";?>">
    <span class="sr-only"></span> -->
</div>
</div></td>
                                                    
                                                </tr>
                                               <?php

                                               }
                                               ?>
                                            
                                            
                                        </tbody>
                                    </table>
                              

    </div>
   </div>
   </div>
   </div>
   </div>
   </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">created with all love in the world by @Ankit and @Osho.</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script src="js/bootstrap.min.js"></script>
  </body>
</html>
