<?php
require_once 'session.php';
if($_SESSION['role']!='s')
header('Location:index.php?id=Login With Proper Credentials to Continue');
?>

?>
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Code-Y-Gen</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/footer1.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <?php require_once 'top_panel_student.php';?>

    <!-- Begin page content -->
    <div class="container">
             <form name="form1" role="form" class="form-horizontal"method="POST" action="update_activity.php">



      <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Ignitron
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
<h2> Activity Hours 15 </h2>
If you have attended ignitron, click yes. we'll check out our databases and award you hours for it.
         <div class="form-group">
         <label for="inputEmail3" class="col-sm-1 control-label"> Description</label>
    <div class="col-sm-10">
    <input type="text" name="1" class="form-control" id="exampleInputEmail1" placeholder="Description">
  </div>
  </div>
</div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Hosting and Running a website
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
       <h2> Activity Hours 30 </h2>
If you have hosted any website, please give us your URL.
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> URL</label>
    <div class="col-sm-10">
      <input type="text"  name="2" class="form-control" id="inputEmail3" placeholder="URL">
    </div>
  </div>
</div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
           Developing any mobile application Windows apps/Android apps
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 30 </h2>
If you have published any windows phone app/android app or any other app , please give us your URL.
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> URL</label>
    <div class="col-sm-10">
      <input type="text" name="3" class="form-control" id="inputEmail3" placeholder="URL">
    </div>
  </div>


         </div>
    </div>
  </div>

<!-- 4th -->

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
           PHP Fest</a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 20 </h2>
If you have organised any PHP fest, please give us the time and venue and other relevant details.
<small>
maximum 200 characters.
</small>
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> Description</label>
    <div class="col-sm-10">
      <input type="text" name="4" class="form-control" id="inputEmail3" placeholder="Description">
    </div>
  </div>


         </div>
    </div>
  </div>


<!-- 5th -->

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
           Organizing Idea Fest</a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 15 </h2>
If you have organised any Idea fest, please give us the time and venue and other relevant details.
<small>
maximum 200 characters.
</small>
<br> <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> Description</label>
    <div class="col-sm-10">
      <input type="text" name="5" class="form-control" id="inputEmail3" placeholder="URL">
    </div>
  </div>


         </div>
    </div>
  </div>
  <!-- 6th -->

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
           Software Expo</a>
      </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 10 </h2>
If you have presented any of your project at Software Expo, please give us the time and venue and other relevant details.
<small>
maximum 200 characters.
</small>
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> Description</label>
    <div class="col-sm-10">
      <input type="text" name="6" class="form-control" id="inputEmail3" placeholder="URL">
    </div>
  </div>


         </div>
    </div>
  </div>
  <!-- 7th -->

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
           Organizing Technical Training</a>
      </h4>
    </div>
    <div id="collapseSeven" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 10 </h2>
If you have organised any technical training, please give us the time and venue and other relevant details.
<small>
maximum 200 characters.
</small>
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> Description</label>
    <div class="col-sm-10">
      <input type="text"  name="7" class="form-control" id="inputEmail3" placeholder="URL">
    </div>
  </div>


         </div>
    </div>
  </div>
  <!-- 8th -->

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
           Organizing Mock Interview
        </a>
      </h4>
    </div>
    <div id="collapseEight" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 10 </h2>
If you have organised any Mock Interview, please give us the time and venue and other relevant details.
<small>
maximum 200 characters.
</small>
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label">Description</label>
    <div class="col-sm-10">
      <input type="text" name="8" class="form-control" id="inputEmail3" placeholder="URL" >
    </div>
  </div>


         </div>
    </div>
  </div>
  <!-- 9th -->

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
           Organizing Online Test</a>
      </h4>
    </div>
    <div id="collapseNine" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 10 </h2>
If you have organised any Online quiz/test, please give us the time and venue and other relevant details.
<small>
maximum 200 characters.
</small>
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> Description</label>
    <div class="col-sm-10">
      <input type="text" name="9" class="form-control" id="inputEmail3" placeholder="Description" >
    </div>
  </div>


         </div>
    </div>
  </div>
  <!-- 10 collapnsete change to colllapnseten  -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTe">
           Tornado (not availabe yet)</a>
      </h4>
    </div>
    <div id="collapseTen" class="panel-collapse collapse">
      <div class="panel-body">
        <h2> Activity Hours 20 </h2>
If you have published any windows phone app , please give us your URL.
<br>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label"> URL</label>
    <div class="col-sm-10">
      <input type="text" name="10" class="form-control" id="inputEmail3" placeholder="URL">
    </div>
  </div>


         </div>
    </div>
  </div>


<br>
<br>
<br>
<button type="submit" class="btn btn-lg btn-success btn-block">Submit</button>

</div>





    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">created with all love in the world by @Ankit and @Osho.</p>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
